# -*- coding: utf-8 -*
import os
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_hasSideBarText():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('其他') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1


# 2. [Screenshot] Side Bar Text
def test_screenSideBarText():
	os.system('adb shell screencap -p /sdcard/screen_Sidebar.png && \
			   adb pull /sdcard/screen_Sidebar.png')
	os.system('adb shell input keyevent 4')

# 3. [Context] Categories
def test_hasCategories():
	os.system('adb shell input swipe 500 1000 500 500')
	time.sleep(1)
	os.system('adb shell input tap 1000 1000')

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1


# 4. [Screenshot] Categories
def test_screenCategories():
	os.system('adb shell screencap -p /sdcard/screen_Categories.png && \
			adb pull /sdcard/screen_Categories.png')
	os.system('adb shell input tap 300 1750')
	time.sleep(1)


# 5. [Context] Categories page
def test_hasCategoriesPage():
	os.system('adb shell input tap 300 1750')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1

# 6. [Screenshot] Categories page
def test_screenCategoriesPage():
	os.system('adb shell screencap -p /sdcard/screen_CategoriesPage.png && \
 			   adb pull /sdcard/screen_CategoriesPage.png')


# 7. [Behavior] Search item “switch”
def test_searchItem():
	os.system('adb shell input tap 300 1750')	# click categories
	time.sleep(1)
	os.system('adb shell input tap 300 150')	# click input
	time.sleep(1)
	os.system('adb shell input text "switch"')
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(4)

# 8. [Behavior] Follow an item and it should be add to the list
def test_followItem():
	os.system('adb shell input tap 300 750')	# click the item
	time.sleep(4)
	os.system('adb shell input tap 150 1750') 	# click the follow button
	time.sleep(2)
	os.system('adb shell input tap 100 100')	# click back
	time.sleep(2)
	os.system('adb shell input tap 100 1750')	# click main page
	time.sleep(2)
	os.system('adb shell input tap 100 100')	# click sidebar
	time.sleep(2)
	os.system('adb shell input tap 380 850')	# click follow list
	time.sleep(4)
	
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')

	

# 9. [Behavior] Navigate tto the detail of item
def test_navigateDetail():
	os.system('adb shell input tap 300 1000')					# click the item
	time.sleep(3)
	os.system('adb shell input swipe 500 1000 500 100')			# swipe down
	time.sleep(2)
	os.system('adb shell input tap 600 100')					# click detail
	time.sleep(2)


# 10. [Screenshot] Disconnetion Screen
def test_screenDisconnection():
	os.system('adb shell input swipe 500 0 500 1500')		# swipe down toolbar
	time.sleep(1)
	os.system('adb shell input tap 1000 300')				# turn-on flight mode
	time.sleep(1)
	os.system('adb shell input keyevent 4')
	time.sleep(1)
	os.system('adb shell screencap -p /sdcard/screen_Disconnection.png && \
			   adb pull /sdcard/screen_Disconnection.png')

